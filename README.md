# kwaeri-react-app

[![pipeline status](https://gitlab.com/mmod/kwaeri-react-app/badges/master/pipeline.svg)](https://gitlab.com/mmod/kwaeri-react-app/commits/master)  [![coverage report](https://gitlab.com/mmod/kwaeri-react-app/badges/master/coverage.svg)](https://mmod.gitlab.io/kwaeri-react-app/coverage/)

A project template for creating front-end (client-side) applications using React.js..

## TOC
* [Implementation Details](#implementation-details)
* [Build Instructions](#build-instructions)
  * [Git the Source](#git-the-source)
  * [Install Dependencies](#install-dependencies)
  * [Build the Application](#build-the-application)
  * [Test the application](#test-the-application)
* [Other Considerations](#other-considerations)
  * [Gitflow](#gitflow)
  * [Continuous Integration and Deployment](#continuous-integration-and-deployment)
  * [FGB: A React Challenge](#fgb-a-react-challenge)
* [Tools](#tools)

## Implementation Details

The application shell consists of the following:

* NPM for project dependency management.
* Webpack for source compilation/optimization.
* Gulp for project maintainence.
* Bootstrap/Material Design for complimenting a standard in design.
* React.js as the primary view layer.
  * Redux may be leveraged as a store for internal state management.
  * GraphQL may be leveraged for intermediating persitent data, as well as for internal state management.
* nkm for a general project cli with support for React (**WIP**)

## Build Instructions

The following steps should be taken in order to build this application:

### Git the Source

Clone the repository from [Gitlab](https://gitlab.com/mmod/kwaeri-react-app):

*via HTTPS*:

```bash
git clone https://gitlab.com/mmod/kwaeri-react-app
```

*via Git+SSH*:

```bash
git clone git@gitlab.com:mmod/kwaeri-react-app
```

### Install dependencies

The following dependencies are required:

* Node.js (latest version is ideal)
  * NPM, which will come pre-bundled with Node.js.

You may use Yarn if you like, however, NPM will suffice.

Head back to your terminal, and proceed to install the additional dependencies as follows:

```bash
cd kwaeri-react-app
npm install .
```

### Build the Application

Now that you have all the dependencies installed, run the following command to build the application:

```bash
npm run build
```

That's it! You've successfully built the application.

### Test the Application

There are two different ways you can test the application:

#### Development

To test the application as a developer, you may leverage the tests which are provided with the source. The tests are built using [Jest](https://jestjs.io/docs/en/tutorial-react) and [ts-jest](https://github.com/kulshekhar/ts-jest/).

Run the following command:

```bash
npm test
```

Snapshots are provided with the source for the the Jest environment to test against. If you contribute to the code base, make sure that you make any adustments to the tests that are necessary, though, you should rarely need to modify the existing tests. If there *are* any changes to the snapshot(s), you will need to update them and ensure that they are included in your commits. You can do this by either:

* Deleting the `__snapshots__` directories for any components whose snapshots have been modified.
* Running the `jest --updateSnapshot` command to re-record every snapshot that fails.
  * Please note that you'll need to either script this through NPM, or install jest-cli.

#### Production

Open `public/index.html` in your favorite browser (*Chrome recommended*) to test the application.

## Other Considerations

As a compliment to the code itself, you could also make use of some common features of Gitlab for performing common project management tasks, as well as in making use of best practices.

To do so, leverage the Project Management features of Gitlab in the following ways:

### Gitflow

* The [project board](https://gitlab.com/mmod/kwaeri-react-app/boards?=) can be leveraged for creating and managing [user-sceanrios/tasks](https://gitlab.com/mmod/kwaeri-react-app/issues), and [milestones](https://gitlab.com/mmod/kwaeri-react-app/milestones) to guide the completion of your project.
  * [Issue-First Development](https://docs.gitlab.com/ee/workflow/gitlab_flow.html) is a perfect candidate for driving the management of your application.

### Continuous Integration and Deployment

Leverage the job/pipeline features of Gitlab in the following ways:

* When checking in the master branch, a [pipline](https://gitlab.com/mmod/kwaeri-react-app/pipelines) may be utilized in testing, building, and deploying your application.
  * [Jobs](https://gitlab.com/mmod/kwaeri-react-app/-/jobs) may be dispatched with every commit to master; Test and Deploy. [Here](https://gitlab.com/mmod/kwaeri-react-app/blob/master/.gitlab-ci.yml)'s an example configuration.

### FGB: A React Challenge

You can review the [Kwaeri React Application](https://mmod.gitlab.io/kwaeri-react-app/), continuously deployed to Gitlab pages upon a check-in to master.

## Tools

The tools leveraged in building this template include:

* [Visual Studio Code](https://code.visualstudio.com/)