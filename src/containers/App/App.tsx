/*------------------------------------------------------------------------------
 * @package:   kwaeri-react-app
 * @author:    Richard B Winters
 * @copyright: 2018 Massively Modified, Inc..
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";
import { connect } from "react-redux";
import { MyKwaeriApp } from "../../components/MyKwaeriApp";


// DEFINES
export interface AppProps { title?: string; }

interface StateFromProps {}

const mapStateToProps = ( state: any ) =>
{
    return {};
};


/**
* App container component
*
* @since 0.1.0
*/
export class App extends React.Component<AppProps, any>
{
   /**
    * @vqar { string } displayName Always set the display name
    *
    * @since 0.1.0
    */
   displayName = "AppContainer";


   /**
    * We'd set propTypes here if this wasn't Typescript.
    */


   /**
    * @var { any } defaultProps The App's default property values
    *
    * @since 0.1.0
    */
   defaultProps = {
       title: "MyKwaeriApp"
   }


   /**
    * Constructor
    *
    * @param props
    *
    * @since 0.1.0
    */
   constructor( props?: AppProps )
   {
       super( props );
   }


   /**
    * Container components pass rendering responsibilities off to
    * presentational components
    *
    * @param { void }
    *
    * @since 0.1.0
    */
   render()
   {
       return (
            <MyKwaeriApp />
        );
   }
}


// Here we wrap the MessageBoardCotnainer with a Connect container,
// creating the MessageBoard Container:
export default connect<StateFromProps>(
   mapStateToProps
)( App );
