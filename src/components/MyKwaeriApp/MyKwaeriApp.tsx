/*------------------------------------------------------------------------------
 * @package:   kwaeri-react-app
 * @author:    Richard B Winters
 * @copyright: 2018 Massively Modified, Inc..
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES
import { styles } from "./";
import * as React from "react";
import { Switch, Route, HashRouter } from "react-router-dom";
import { UnderConstruction } from "../UnderConstruction";;
import { ErrorHandler } from "../ErrorHandler";


// DEFINES
export interface MyKwaeriAppProps { title?: string; }


/**
 * MyKwaeriApp component
 *
 * @param { MyKwaeriAppProps } props
 *
 * @since 0.1.0
 */
export const MyKwaeriApp: React.SFC<MyKwaeriAppProps> = ( props ) =>
{
    // Here we render the component:
    return (
        <HashRouter>
            <div className={styles.kwaeriAppContainerInner}>
                <div className={"row " + styles.contentBlock}>
                    <div className={"col-md-12 "  + styles.contentBlockInner}>
                        <Switch>
                            <Route exact path="/" component={UnderConstruction}/>
                            <Route component={ErrorHandler} />
                        </Switch>
                    </div>
                </div>
            </div>
        </HashRouter>
    );
}


/**
 * @var { string } displayName Always set the display name
 *
 * @since 0.1.0
 */
MyKwaeriApp.displayName = "MyKwaeriApp";


/**
 * We'd set the propTypes here if this wasn't Typescript.
 * We'd set the defaultProps here if we wanted to.
 */
