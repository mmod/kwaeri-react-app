/*------------------------------------------------------------------------------
 * @package:   kwaeri-react-app
 * @author:    Richard B Winters
 * @copyright: 2018 Massively Modified, Inc..
 * @license:   Apache v2.0
 * @version:   1.0.0
 *----------------------------------------------------------------------------*/


// INCLUDES


// DEFINES



// Constants:

/* The ADD_POST constant: */
export const ADD_POST = "ADD_POST";

/* The UPDATE_POST constant: */
export const UPDATE_POST = "UPDATE_POST";
